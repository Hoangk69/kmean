import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.datasets import load_iris

iris = load_iris()

data = iris['data'][:,:2]

# Áp dụng thuật toán K-Means với số lượng cụm là 3
kmeans = KMeans(n_clusters=3)
cluster_labels = kmeans.fit_predict(data)

# tâm cụm
centers = kmeans.cluster_centers_

plt.scatter(data[:, 0], data[:, 1], c=cluster_labels, marker='.')
plt.scatter(centers[:, 0], centers[:, 1], s=200, marker='x', c='red')
plt.title('Biểu đồ phân cụm sử dụng K-Means')
plt.xlabel('sepal length (cm)')
plt.ylabel('sepal width (cm)')
plt.show()