
# Kmean
Phân cụm K-means là một thuật toán phân cụm dữ liệu thống kê được sử dụng để nhóm các điểm dữ liệu thành các cụm (clusters) dựa trên các đặc trưng tương tự. Mục tiêu của thuật toán này là tìm ra các cụm mà mỗi điểm dữ liệu trong cùng một cụm giống nhau và các cụm có sự khác biệt lớn nhất với nhau.

Nhược điểm: cần biết trước số cụm cần phân chia. (có thể sử dụng phương pháp "Elbow" hoặc phương pháp "Silhouette").

- Ưu tiên phương pháp Silhouette hơn vì dữ liệu phức việc tạp xác định điểm khuỷu tay qua phương pháp Elbow thì khá là khó.

![Alt text](image.png)
[xem thêm nguồn hình ảnh](https://d-nb.info/1230738193/34)

## Mô tả thuật toán

_**Bước 1**_: Cần xác định số cụm cần phân chia K=?.

_**Bước 2**_: Khởi tạo tâm cụm ban đầu: bằng cách chọn ngẫu nhiên K điểm

_**Bước 3**_: Phân cụm: Gán mỗi điểm dữ liệu vào cụm có tâm cụm gần nó nhất.

_**Bước 4**_: Cập nhật tâm cụm: Tính trung tâm mới cho mỗi cụm bằng cách lấy trung bình của tất cả các điểm dữ liệu thuộc cụm đó.

_**Bước 5**_: Lặp bước 3 và 4 cho đến khi không có sự thay đổi đáng kể nào nữa hoặc đạt đến số lần lặp cố định.

## Code hai phương pháp để nhận về số K hợp lý.

- [ ] silhouette.ipynb (phương pháp hệ số bóng)
- [ ] elbow.ipynb (phương pháp khuỷu tay)

## Input truyền vào

- [ ] dataset từ thư viện sklearn.datasets hoa iris().
- [ ] Sample: 150 (số lượng điểm)
- [ ] Feature names: ['sepal length (cm)', 'sepal width (cm)', 'petal length (cm)', 'petal width (cm)'] (gồm 4 đặc tính)

## Runtime

- [ ] Python 3.12.0

## Install package trường hợp sử dụng IDE Visual Studio Code, cách check version package 
- **Install package**

```
py -m pip install ten_package
```

- **Check version package**

```
py -m pip list
```

# Setup môi trường

[Xem thêm tại đây.](https://www.datacamp.com/tutorial/setting-up-vscode-python?utm_source=google&utm_medium=paid_search&utm_campaignid=19589720824&utm_adgroupid=157156376311&utm_device=c&utm_keyword=&utm_matchtype=&utm_network=g&utm_adpostion=&utm_creative=680291483907&utm_targetid=dsa-2218886984100&utm_loc_interest_ms=&utm_loc_physical_ms=1028580&utm_content=&utm_campaign=230119_1-sea~dsa~tofu_2-b2c_3-row-p2_4-prc_5-na_6-na_7-le_8-pdsh-go_9-na_10-na_11-na-bfcm23&gad_source=1&gclid=Cj0KCQiAsburBhCIARIsAExmsu66YNBVrhKRCjoimB0Ob_wAfm8mt9uy1IkXSz2_XT1m2B-3aRmiiJcaAsakEALw_wcB)
